/**
 * @file
 * Implements SearchStax tracking functionality.
 */

/* eslint-disable func-names */

// General SearchStax tracking code.
// eslint-disable-next-line no-var, no-use-before-define
var _msq = _msq || []; // declare object

(function () {
  const ms = document.createElement('script');
  ms.type = 'text/javascript';
  const jsVersion =
    typeof drupalSettings !== 'undefined' &&
    drupalSettings.searchstax &&
    drupalSettings.searchstax.js_version;
  switch (jsVersion) {
    case '2':
      ms.src = 'https://static.searchstax.com/js/ms2.js';
      break;

    case '3':
    default:
      ms.src =
        'https://static.searchstax.com/studio-js/v3/js/studio-analytics.js';
      window.analyticsBaseUrl =
        (typeof drupalSettings !== 'undefined' &&
          drupalSettings.searchstax &&
          drupalSettings.searchstax.analytics_url) ||
        'https://app.searchstax.com';
      break;
  }
  const s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(ms, s);
})();

// Drupal-specific tracking code.
(function ($) {
  Drupal.searchstax = Drupal.searchstax || {};

  /**
   * Click handler for search result links.
   */
  Drupal.searchstax.trackSearchResults = function () {
    if (
      !drupalSettings ||
      !drupalSettings.searchstax ||
      !drupalSettings.searchstax.searches ||
      typeof _msq === 'undefined'
    ) {
      return;
    }

    const baseData = drupalSettings.searchstax.tracking_base_data;
    if (!baseData.session) {
      // This is an anonymous user, without fixed session. Generate a new random
      // session key for just this page.
      // See https://attacomsian.com/blog/javascript-generate-random-string.

      // Declare all characters.
      const chars =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

      // Pick characters randomly.
      baseData.session = '';
      for (let i = 0; i < 32; i++) {
        baseData.session += chars.charAt(
          Math.floor(Math.random() * chars.length),
        );
      }
    }
    $.each(drupalSettings.searchstax.searches, function () {
      // Don't track the same result set twice.
      if (this.tracked) {
        return;
      }
      this.tracked = true;
      const track = $.extend({}, baseData, this);
      _msq.push(['track', track]);
    });
  };

  /**
   * Click handler for search result links.
   */
  Drupal.searchstax.trackClick = function () {
    const linkElement = this;
    const $link = $(this);
    const position = $link.data('searchstax-position');
    const searchId = $link.data('searchstax-search');
    const { searches } = drupalSettings.searchstax;
    if (!searches[searchId] || !searches[searchId].impressions) {
      return;
    }
    $.each(searches[searchId].impressions, function () {
      if (this.position !== position) {
        return;
      }

      const baseData = drupalSettings.searchstax.tracking_base_data;
      const track = $.extend({}, baseData, this, {
        query: searches[searchId].query,
        pageNo: searches[searchId].pageNo,
        shownHits: searches[searchId].shownHits,
        totalHits: searches[searchId].totalHits,
        pageUrl: linkElement.href,
      });
      _msq.push(['trackClick', track]);
      return false;
    });
  };

  /**
   * Enables tracking for clicks on all search results links.
   */
  Drupal.behaviors.searchstax = {
    attach(context, settings) {
      if (
        !settings ||
        !settings.searchstax ||
        !settings.searchstax.searches ||
        typeof _msq === 'undefined'
      ) {
        return;
      }

      // First off, track all search results (that weren't tracked already).
      Drupal.searchstax.trackSearchResults();

      if (!settings.searchstax.results_urls) {
        return;
      }

      const resultUrls = drupalSettings.searchstax.results_urls;
      let $searchResults = $('[data-searchstax-results]', context);
      // For AJAX responses, "context" itself might be a search results listing.
      const $context = $(context);
      if ($context.data('searchstax-results')) {
        $searchResults = $searchResults.add($context);
      }
      $searchResults.each(function () {
        const $resultsArea = $(this);
        const searchId = $resultsArea.data('searchstax-results');
        if (!resultUrls[searchId]) {
          return;
        }
        $.each(resultUrls[searchId], function () {
          $resultsArea
            .find(`a[href="${this.url}"]`)
            .data('searchstax-position', this.position)
            .data('searchstax-search', searchId)
            .click(Drupal.searchstax.trackClick);
        });
      });
    },
  };
})(jQuery);
