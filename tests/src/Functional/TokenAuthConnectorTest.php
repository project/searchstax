<?php

declare(strict_types=1);

namespace Drupal\Tests\searchstax\Functional;

use Drupal\Component\Serialization\Yaml;
use Drupal\search_api\Entity\Index;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\search_api\Functional\ExampleContentTrait;
use Drupal\views\Entity\View;

/**
 * Tests the Solr connector plugin functionality.
 *
 * @group searchstax
 *
 * @coversDefaultClass \Drupal\searchstax\Plugin\SolrConnector\SearchStaxConnector
 */
class TokenAuthConnectorTest extends BrowserTestBase {

  use ExampleContentTrait;
  use TestAssertionsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    'search_api_solr',
    'search_api_test_example_content',
    'searchstax',
    'searchstax_test_mock_http',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setUpExampleStructure();
    $this->insertExampleContent();

    // Set a predictable default timezone and site hash.
    \Drupal::configFactory()->getEditable('system.date')
      ->set('timezone.default', 'Europe/Vienna')
      ->save(TRUE);
    \Drupal::state()->set('search_api_solr.site_hash', '12rxq3');

    // Prepare the mock HTTP client.
    \Drupal::keyValue('searchstax_test_mock_http')
      ->set('data_dir', __DIR__ . '/../../data/token-auth');
  }

  /**
   * Tests the Solr connector plugin functionality.
   */
  public function testTokenAuthSolrConnector(): void {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->drupalCreateUser([], NULL, TRUE));

    // Enable tracking to be able to test this as well.
    $this->drupalGet('admin/config/search/searchstax');
    $this->submitForm([
      'analytics_url' => 'https://example.com',
      'analytics_key' => 'test_analytics_key_view',
    ], 'Save configuration');
    $assert_session->pageTextContains('The configuration options have been saved.');
    $this->assertNoWarningsLogged();

    // Add a search server.
    $this->drupalGet('admin/config/search/search-api/add-server');
    $assert_session->pageTextContains('Add search server');
    $this->assertNoWarningsLogged();

    $this->submitForm([
      'name' => 'SearchStax server',
      'id' => 'searchstax_server',
      'backend_config[connector]' => 'searchstax',
    ], 'Save');
    $assert_session->pageTextContains('Please configure the selected Solr connector.');
    $this->assertNoWarningsLogged();

    $this->submitForm([
      'backend_config[connector_config][update_endpoint]' => 'https://searchcloud.example.searchstax.com/1234/firstapp-100/update',
      'backend_config[connector_config][update_token]' => '9232215801aa9e201c1afa76270eb8065aa6964a',
    ], 'Save');
    $assert_session->pageTextContains('The server was successfully saved.');
    $this->assertPageRequestWasSuccessful();

    // Add a test index and view.
    $index_file = __DIR__ . '/../../modules/searchstax_test/config/install/search_api.index.searchstax_index.yml';
    $index_values = Yaml::decode(file_get_contents($index_file));
    Index::create($index_values)->save();
    $view_file = __DIR__ . '/../../modules/searchstax_test/config/install/views.view.searchstax_test_view.yml';
    $view_values = Yaml::decode(file_get_contents($view_file));
    View::create($view_values)->save();
    drupal_flush_all_caches();

    // Visit the view (which should be empty).
    $this->drupalGet('test-search-view');
    $assert_session->pageTextContains('Displaying 0 search results');
    $this->assertPageRequestWasSuccessful();

    // Visit the index page and index all items.
    $this->drupalGet('admin/config/search/search-api/index/searchstax_index');
    $this->submitForm([], 'Index now');
    $assert_session->pageTextContains('Successfully indexed 5 items.');
    $this->assertPageRequestWasSuccessful();

    // Visit the view again but with keywords.
    $this->drupalGet('test-search-view', ['query' => ['search_api_fulltext' => 'test']]);
    $assert_session->pageTextContains('Displaying 4 search results');
    $this->assertPageRequestWasSuccessful();
    $this->assertCurrentPageContainsTracking();
  }

}
