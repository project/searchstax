<?php

declare(strict_types=1);

namespace Drupal\Tests\searchstax\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the SettingsForm functionality.
 *
 * @group searchstax
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['searchstax'];

  /**
   * Tests that the settings form saves configuration.
   */
  public function testSettingsFormSave() {
    // Create a user with permission to administer the site configuration.
    $this->drupalLogin($this->drupalCreateUser(['administer site configuration']));

    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    // Visit the settings page.
    $this->drupalGet('/admin/config/search/searchstax');
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('SearchStax settings');

    // Check if the fields exist.
    $assert->fieldExists('js_version');
    $assert->fieldExists('analytics_url');
    $assert->fieldExists('analytics_key');
    $assert->fieldExists('untracked_roles[anonymous]');
    $assert->fieldExists('untracked_roles[authenticated]');
    $assert->fieldExists('searches_via_searchstudio');
    $assert->fieldExists('configure_via_searchstudio');
    $assert->fieldExists('discard_parameters[keys]');
    $assert->fieldExists('discard_parameters[highlight]');

    // Fill the fields.
    $page->fillField('js_version', '3');
    $page->fillField('analytics_url', 'https://example.com/analytics');
    $page->fillField('analytics_key', 'test_key');
    $page->checkField('untracked_roles[anonymous]');
    $page->checkField('searches_via_searchstudio');
    $page->checkField('configure_via_searchstudio');
    $page->checkField('discard_parameters[keys]');
    $page->checkField('discard_parameters[highlight]');

    // Submit the form.
    $page->pressButton('Save configuration');
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('SearchStax settings');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Verify that the configuration was saved.
    $config = $this->config('searchstax.settings');
    $this->assertEquals('3', $config->get('js_version'));
    $this->assertEquals('https://example.com/analytics', $config->get('analytics_url'));
    $this->assertEquals('test_key', $config->get('analytics_key'));
    $this->assertEquals(['anonymous'], $config->get('untracked_roles'));
    $this->assertTrue($config->get('searches_via_searchstudio'));
    $this->assertTrue($config->get('configure_via_searchstudio'));
    $this->assertEquals(['highlight', 'keys', 'sort', 'spellcheck'], $config->get('discard_parameters'));

    // Login as an anonymous user.
    $this->drupalLogout();
    // Test with an anonymous user.
    $this->assertTrue($this->container->get('searchstax.utility')->isTrackingDisabled());

    // Login as an authenticated user.
    $this->drupalLogin($this->drupalCreateUser());
    // Test with an authenticated user.
    $this->assertFalse($this->container->get('searchstax.utility')->isTrackingDisabled());
  }

}
