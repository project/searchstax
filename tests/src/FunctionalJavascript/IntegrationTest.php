<?php

namespace Drupal\Tests\searchstax\FunctionalJavascript;

use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Element\NodeElement;
use Drupal\Core\Session\AccountInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\search_api_autocomplete\Entity\Search;
use Drupal\search_api_test\PluginTestTrait;
use Drupal\Tests\search_api\Functional\ExampleContentTrait;
use Drupal\Tests\searchstax\Functional\TestAssertionsTrait;
use Drupal\user\Entity\Role;

// cspell:ignore goutte

/**
 * Tests the "SearchStax" autocomplete suggester plugin.
 *
 * @group searchstax
 */
class IntegrationTest extends WebDriverTestBase {

  use ExampleContentTrait;
  use PluginTestTrait;
  use TestAssertionsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api',
    'search_api_autocomplete',
    'search_api_page',
    'search_api_solr',
    'search_api_test_example_content',
    'searchstax',
    'searchstax_test',
    'dblog',
    'field_ui',
    'image',
    'link',
    'node',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user used in this test.
   */
  protected AccountInterface $adminUser;

  /**
   * The role of the admin user used in this test.
   */
  protected string $adminRole;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Set up example content.
    $this->setUpExampleStructure();
    $this->insertExampleContent();
    // Give example entity 3 a more distinctive name since it's otherwise harder
    // to check whether or not it is part of a search result.
    $this->entities[3]->set('name', 'foobar foobar baz')->save();

    // Create an admin role and user.
    $permissions = [
      'administer site configuration',
      'administer search_api',
      'access administration pages',
      'administer nodes',
      'bypass node access',
      'administer content types',
      'administer node fields',
    ];
    $this->adminRole = $this->createRole($permissions);
    $this->adminUser = $this->drupalCreateUser([], NULL, FALSE, [
      'roles' => [$this->adminRole],
    ]);

    // Everyone should be able to view search pages and test entities.
    foreach ([Role::ANONYMOUS_ID, Role::AUTHENTICATED_ID] as $role_id) {
      $this->grantPermissions(Role::load($role_id), [
        'view search api pages',
        'view test entity',
        'view test entity translations',
      ]);
    }

    drupal_flush_all_caches();
  }

  /**
   * Tests all module functionality.
   */
  public function testModule(): void {
    $web_assert = $this->assertSession();

    // At first, all SearchStax functionality is disabled so we're expecting no
    // tracking and regular Solr requests.
    // Search view request with empty keys:
    $q = urlencode('*:*');
    $uri_regex_empty_keys = "#^select\\?(.+&)?q=$q#";
    $this->addExpectedSolrRequests($uri_regex_empty_keys);
    $view_request_empty_keys = $this->assertSearchViewTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertSolrRequestMatches(
      '*:*',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
        'sort' => 'ss_search_api_id asc',
        'hl' => 'true',
        'facet' => 'true',
        'spellcheck' => 'true',
      ],
      ['qf', 'defType'],
      $view_request_empty_keys
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();

    // Search page request with empty keys:
    $page_request_empty_keys = $this->assertSearchPageTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertSolrRequestMatches(
      '*:*',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
        'hl' => 'true',
        'facet' => 'true',
        'spellcheck' => 'true',
      ],
      ['qf', 'defType'],
      $page_request_empty_keys
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();

    // Search view request for keyword "foo":
    $uri_regex_foo_keys = '#^select\\?(.+&)?q=[^&]*foo#';
    $uri_regex_foo_keys_original = $uri_regex_foo_keys;
    $foo_results = [1, 2, 4, 5];
    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results);
    $view_request_foo = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $view_request_foo_original = $view_request_foo;
    $this->assertSolrRequestMatches(
      'foo',
      FALSE,
      [
        'start' => '0',
        'rows' => '10',
        'sort' => 'ss_search_api_id asc',
        'hl' => 'true',
        'facet' => 'true',
        'spellcheck' => 'true',
      ],
      ['qf', 'defType'],
      $view_request_foo
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // Search page request for keyword "foo":
    $page_request_foo = $this->assertSearchPageTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertSolrRequestMatches(
      'foo',
      FALSE,
      [
        'start' => '0',
        'rows' => '10',
        'hl' => 'true',
        'facet' => 'true',
        'spellcheck' => 'true',
      ],
      ['qf', 'defType'],
      $page_request_foo
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // Now activate tracking and see whether that works as intended.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/search');
    $this->clickLink('SearchStax settings');
    $web_assert->fieldValueEquals('js_version', '3');
    $web_assert->fieldValueEquals('analytics_url', 'https://app.searchstax.com');
    $web_assert->checkboxNotChecked('searches_via_searchstudio');
    $this->click('details[data-drupal-selector="edit-search-specific-analytics-keys"] summary[role="button"]');
    $this->submitForm([
      'analytics_url' => 'https://example.com',
      'analytics_key' => 'test_analytics_key_view',
      'search_specific_analytics_keys[search_api_page:searchstax_test_search]' => 'test_analytics_key_page',
      'search_specific_analytics_keys[views_page:other_solr_test_view__page_1]' => 'test_analytics_key_other_view',
      'autosuggest_core' => 'searchstax-test_suggester',
    ], 'Save configuration');

    // Check the configuration was successfully updated.
    $expected = [
      'js_version' => '3',
      'analytics_url' => 'https://example.com',
      'analytics_key' => 'test_analytics_key_view',
      'search_specific_analytics_keys' => [
        'search_api_page:searchstax_test_search' => 'test_analytics_key_page',
        'views_page:other_solr_test_view__page_1' => 'test_analytics_key_other_view',
      ],
      'autosuggest_core' => 'searchstax-test_suggester',
      'searches_via_searchstudio' => FALSE,
    ];
    $actual = \Drupal::config('searchstax.settings')->get();
    $this->assertEquals($expected, array_intersect_key($actual, $expected));

    // Now that the "autosuggest_core" is set, we can create the Autocomplete
    // search.
    (Search::create([
      'id' => 'searchstax_test_view',
      'langcode' => 'en',
      'status' => TRUE,
      'label' => 'SearchStax search',
      'index_id' => 'searchstax_index',
      'suggester_settings' => [
        'searchstax' => [],
      ],
      'search_settings' => [
        'views:searchstax_test_view' => [],
      ],
      'options' => [
        'show_count' => TRUE,
      ],
    ]))->save();
    $this->grantPermissions(Role::load(Role::ANONYMOUS_ID), ['use search_api_autocomplete for searchstax_test_view']);
    $this->grantPermissions(Role::load(Role::AUTHENTICATED_ID), ['use search_api_autocomplete for searchstax_test_view']);

    // Visit the pages again. The page with keywords should contain tracking JS.
    $this->drupalLogout();
    $this->addExpectedSolrRequests($uri_regex_empty_keys);
    $request = $this->assertSearchViewTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertEquals($view_request_empty_keys, $request);
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();
    $request = $this->assertSearchPageTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertEquals($page_request_empty_keys, $request);
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();

    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results);
    $request = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($view_request_foo, $request);
    $this->assertCurrentPageContainsTracking('view', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);
    $request = $this->assertSearchPageTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($page_request_foo, $request);
    $this->assertCurrentPageContainsTracking('page', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // Do the same while logged in as admin, which should currently not make a
    // difference.
    $this->drupalLogin($this->adminUser);
    $this->addExpectedSolrRequests($uri_regex_empty_keys);
    $request = $this->assertSearchViewTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertEquals($view_request_empty_keys, $request);
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();
    $request = $this->assertSearchPageTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertEquals($page_request_empty_keys, $request);
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();

    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results);
    $request = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($view_request_foo, $request);
    $this->assertCurrentPageContainsTracking('view', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);
    $request = $this->assertSearchPageTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($page_request_foo, $request);
    $this->assertCurrentPageContainsTracking('page', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // Enable the "Configure searches via SearchStudio" option and disable
    // tracking for admins.
    $this->drupalGet('admin/config/search/searchstax');
    $this->submitForm([
      "untracked_roles[{$this->adminRole}]" => TRUE,
      'searches_via_searchstudio' => TRUE,
      'configure_via_searchstudio' => TRUE,
      'discard_parameters[keys]' => TRUE,
      'discard_parameters[highlight]' => TRUE,
      'discard_parameters[spellcheck]' => TRUE,
      'discard_parameters[sort]' => TRUE,
      'discard_parameters[facets]' => TRUE,
    ], 'Save configuration');

    // Check the configuration was successfully updated.
    $expected = [
      'js_version' => '3',
      'analytics_url' => 'https://example.com',
      'analytics_key' => 'test_analytics_key_view',
      'search_specific_analytics_keys' => [
        'search_api_page:searchstax_test_search' => 'test_analytics_key_page',
        'views_page:other_solr_test_view__page_1' => 'test_analytics_key_other_view',
      ],
      'untracked_roles' => [
        $this->adminRole,
      ],
      'autosuggest_core' => 'searchstax-test_suggester',
      'searches_via_searchstudio' => TRUE,
      'configure_via_searchstudio' => TRUE,
      'discard_parameters' => [
        'facets',
        'highlight',
        'keys',
        'sort',
        'spellcheck',
      ],
    ];
    $actual = \Drupal::config('searchstax.settings')->get();
    $this->assertEquals($expected, array_intersect_key($actual, $expected));

    // Adapt the expected URLs accordingly.
    $uri_regex_empty_keys = "#^emselect\\?(.+&)?q=$q#";
    $uri_regex_foo_keys = '#^emselect\\?(.+&)?q=[^&]*foo#';

    // Make sure there now isn't tracking on the "foo" search results page for
    // the admin account.
    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results);
    $view_request_foo = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertSolrRequestMatches(
      'foo',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
      ],
      [
        'facet',
        'hl',
        'qf',
        'sort',
        'spellcheck',
      ],
      $view_request_foo
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults($foo_results);
    $page_request_foo = $this->assertSearchPageTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertSolrRequestMatches(
      'foo',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
      ],
      [
        'facet',
        'hl',
        'qf',
        'spellcheck',
      ],
      $page_request_foo
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // After logging out, tracking should be there again.
    $this->drupalLogout();
    $this->addExpectedSolrRequests($uri_regex_empty_keys);
    $view_request_empty_keys = $this->assertSearchViewTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertSolrRequestMatches(
      '*:*',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
      ],
      [
        'facet',
        'hl',
        'qf',
        'sort',
        'spellcheck',
      ],
      $view_request_empty_keys
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();
    $page_request_empty_keys = $this->assertSearchPageTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertSolrRequestMatches(
      '*:*',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
      ],
      [
        'facet',
        'hl',
        'qf',
        'spellcheck',
      ],
      $page_request_empty_keys
    );
    $this->assertCurrentPageNotContainsTracking();
    $this->assertCurrentPageContainsSearchResults();

    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results);
    $request = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($view_request_foo, $request);
    $this->assertCurrentPageContainsTracking('view', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);
    $request = $this->assertSearchPageTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertEquals($page_request_foo, $request);
    $this->assertCurrentPageContainsTracking('page', $foo_results);
    $this->assertCurrentPageContainsSearchResults($foo_results);

    // Verify that a second, non-SearchStax Solr server is unaffected.
    $this->addExpectedSolrRequests($uri_regex_foo_keys_original, $foo_results, 1, 'solr-test');
    $other_solr_request = $this->assertPageVisitTriggersSolrRequest('other-solr-test-search-view', ['search_api_fulltext' => 'foo'], $uri_regex_foo_keys_original);
    // By just adapting the filter on "index_id" the request should be the same
    // as for the SearchStax view.
    $other_solr_request['fq'] = str_replace('other_solr_index', 'searchstax_index', $other_solr_request['fq']);
    $this->assertEquals($view_request_foo_original, $other_solr_request);
    $this->assertCurrentPageContainsSearchResults($foo_results);
    // Tracking is not tied to a SearchStax (or even Solr) server at all, so
    // there should be tracking on this page.
    $this->assertCurrentPageContainsTracking('other_view', $foo_results);

    // Disable discarding of parse mode and facets again to see if a subset also
    // works fine.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/search/searchstax');
    $this->submitForm([
      'discard_parameters[keys]' => FALSE,
      'discard_parameters[facets]' => FALSE,
    ], 'Save configuration');
    // Check the configuration was successfully updated.
    $expected = [
      'highlight',
      'sort',
      'spellcheck',
    ];
    $actual = \Drupal::config('searchstax.settings')->get('discard_parameters');
    $this->assertEquals($expected, $actual);

    // Log out and check again. (Just the Solr requests of the search view, to
    // not waste too much time.)
    $this->drupalLogout();
    $this->addExpectedSolrRequests($uri_regex_empty_keys, [], 1);
    $view_request_empty_keys = $this->assertSearchViewTriggersSolrRequest(NULL, $uri_regex_empty_keys);
    $this->assertSolrRequestMatches(
      '*:*',
      TRUE,
      [
        'start' => '0',
        'rows' => '10',
        'defType' => 'lucene',
        'facet' => 'true',
      ],
      [
        'hl',
        'qf',
        'sort',
        'spellcheck',
      ],
      $view_request_empty_keys
    );
    $this->addExpectedSolrRequests($uri_regex_foo_keys, $foo_results, 1);
    $view_request_foo = $this->assertSearchViewTriggersSolrRequest('foo', $uri_regex_foo_keys);
    $this->assertSolrRequestMatches(
      'foo',
      FALSE,
      [
        'start' => '0',
        'rows' => '10',
        'defType' => 'lucene',
        'facet' => 'true',
      ],
      [
        'hl',
        'qf',
        'sort',
        'spellcheck',
      ],
      $view_request_foo
    );

    // Finally, check that the autocomplete suggester works correctly.
    $this->addExpectedSolrRequests(
      '#^emsuggest\\?(.+&)?q=bar(&|$)#',
      NULL,
      1,
      'searchstax-test_suggester',
      [
        'suggest' => [
          'studio_suggestor_en' => [
            'foo' => [
              'numFound' => 3,
              'suggestions' => [
                [
                  'term' => '<b>bar</b>n',
                  'weight' => 100,
                  'payload' => '',
                ],
                [
                  'term' => 'ban',
                  'weight' => 75,
                  'payload' => '',
                ],
                [
                  'term' => 'car',
                  'weight' => 50,
                  'payload' => '',
                ],
              ],
            ],
          ],
        ],
      ]
    );

    $assert_session = $this->assertSession();
    $input_selector = 'input[data-drupal-selector="edit-search-api-fulltext"]';
    $assert_session->elementAttributeContains(
      'css',
      $input_selector,
      'data-search-api-autocomplete-search',
      'searchstax_test_view'
    );

    $page = $this->getSession()->getPage();
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $field = $assert_session->elementExists('css', $input_selector);
    $field->setValue('bar');
    $this->getSession()
      ->getDriver()
      ->keyDown($field->getXpath(), 'r');

    $element = $assert_session->waitOnAutocomplete();
    $this->logPageChange('Autocomplete');
    $this->assertNoWarningsLogged();
    $this->assertTrue($element && $element->isVisible());

    // Contrary to documentation, this can also return NULL. Therefore, we need
    // to make sure to return an array even in this case.
    $elements = $page->findAll('css', '.ui-autocomplete .ui-menu-item') ?: [];
    $suggestions = [];
    foreach ($elements as $element) {
      $label = $this->getElementText($element, '.autocomplete-suggestion-label');
      $user_input = $this->getElementText($element, '.autocomplete-suggestion-user-input');
      $suffix = $this->getElementText($element, '.autocomplete-suggestion-suggestion-suffix');
      $suggestions[] = $label . $user_input . $suffix;
    }
    $expected = ['barn', 'ban', 'car'];
    $this->assertEquals($expected, $suggestions);
  }

  /**
   * Adds one or more expected Solr request for the test connector plugin.
   *
   * @param string $uri_regex
   *   A regular expression matching the expected Solr request URI.
   * @param int[]|null $results
   *   The results to return in the Solr response, as their index in
   *   $this->entities. Or NULL to return all items.
   * @param int $count
   *   (optional) The number of requests to expect for this regular expression.
   * @param string $solr_core
   *   (optional) The Solr core to which the request is expected to be sent.
   * @param array|null $response
   *   (optional) The HTTP response to return for the Solr request. If set,
   *   $results will be ignored, otherwise a response will be constructed from
   *   $results.
   *
   * @see \Drupal\searchstax_test\Plugin\SolrConnector\SearchStaxTestSolrConnector::executeRequest()
   */
  protected function addExpectedSolrRequests(
    string $uri_regex,
    ?array $results = NULL,
    int $count = 2,
    string $solr_core = 'searchstax-test',
    ?array $response = NULL
  ): void {
    $results = $results ?? array_keys($this->entities);
    if ($response === NULL) {
      $response = [
        'response' => [
          'numFound' => count($results),
          'start' => 0,
          'numFoundExact' => TRUE,
          'docs' => [],
        ],
      ];
      foreach ($results as $result_key) {
        $response['response']['docs'][] = [
          'ss_search_api_id' => $this->ids[$result_key],
          'ss_search_api_language' => 'en',
          'score' => 1.0,
        ];
      }
    }
    $key_value = \Drupal::keyValue('searchstax_test');
    $expected_requests = $key_value->get('expected_requests', []);
    $expected_requests[] = [
      'regex' => $uri_regex,
      'core' => $solr_core,
      'response' => $response,
      'count' => $count,
    ];
    $key_value->set('expected_requests', $expected_requests);
  }

  /**
   * Asserts visiting the given page triggers a specific Solr request.
   *
   * @param string $path
   *   The path to visit.
   * @param array $query
   *   The GET parameters to use.
   * @param string $uri_regex
   *   A regular expression against which to match the Solr request.
   *
   * @return array
   *   The GET parameters of the Solr request.
   */
  protected function assertPageVisitTriggersSolrRequest(
    string $path,
    array $query,
    string $uri_regex
  ): array {
    $this->drupalGet($path, ['query' => $query]);
    $this->assertNoWarningsLogged();
    $this->assertSession()->pageTextNotContains('An error occurred while searching, try again later.');
    $this->assertSession()->pageTextNotContains('Unexpected Solr request:');
    return $this->assertSolrRequestHappened($uri_regex);
  }

  /**
   * Asserts a search on the test view triggers a specific Solr request.
   *
   * @param string|null $keys
   *   The search keywords to pass, if any.
   * @param string $uri_regex
   *   A regular expression against which to match the Solr request.
   *
   * @return array
   *   The GET parameters of the Solr request.
   *
   * @see views.view.searchstax_test_view.yml
   */
  protected function assertSearchViewTriggersSolrRequest(
    ?string $keys,
    string $uri_regex
  ): array {
    $query = [];
    if ($keys !== NULL) {
      $query['search_api_fulltext'] = $keys;
    }
    return $this->assertPageVisitTriggersSolrRequest('test-search-view', $query, $uri_regex);
  }

  /**
   * Asserts a search on the test search page triggers a specific Solr request.
   *
   * @param string|null $keys
   *   The search keywords to pass, if any.
   * @param string $uri_regex
   *   A regular expression against which to match the Solr request.
   *
   * @return array
   *   The GET parameters of the Solr request.
   *
   * @see search_api_page.search_api_page.searchstax_test_search.yml
   */
  protected function assertSearchPageTriggersSolrRequest(
    ?string $keys,
    string $uri_regex
  ): array {
    $path = 'test-search-page';
    if ($keys !== NULL) {
      $path .= "/$keys";
    }
    return $this->assertPageVisitTriggersSolrRequest($path, [], $uri_regex);
  }

  /**
   * Asserts that a Solr request matching the given URI regex has been made.
   *
   * @param string $uri_regex
   *   The regular expression.
   *
   * @return array
   *   The GET parameters of the Solr request.
   */
  protected function assertSolrRequestHappened(string $uri_regex): array {
    $key_value = \Drupal::keyValue('searchstax_test');
    $seen_requests = $key_value->get('seen_requests', []);
    $requests = [];
    foreach ($seen_requests as $i => $request_uri) {
      if (preg_match($uri_regex, $request_uri)) {
        unset($seen_requests[$i]);
        $key_value->set('seen_requests', array_values($seen_requests));
        [, $params_string] = explode('?', $request_uri, 2) + [1 => ''];
        // Unfortunately, parse_str() doesn't understand the Solr style of
        // multi-valued GET parameters, so we need to do our own parsing.
        $params = [];
        foreach (explode('&', $params_string) as $param_pair) {
          [$key, $value] = array_map('urldecode', explode('=', $param_pair, 2));
          if (!isset($params[$key])) {
            $params[$key] = $value;
          }
          else {
            if (!is_array($params[$key])) {
              $params[$key] = [$params[$key]];
            }
            $params[$key][] = $value;
          }
        }
        return $params;
      }
      $requests[] = $request_uri;
    }
    $requests = $requests ? implode("\n- ", $requests) : '(none)';
    $this->fail("No Solr request recorded that matches regex \"$uri_regex\". Executed requests:\n- $requests");
  }

  /**
   * Asserts that the given Solr request matches the expected.
   *
   * @param string $expected_q
   *   The expected "q" parameter, or a substring expected to be contained in
   *   it.
   * @param bool $q_exact
   *   TRUE if $expected_q should match the "q" parameter exactly, FALSE if it
   *   should be just a substring.
   * @param array $expected_params
   *   An associative array of expected parameters mapped to their values (need
   *   not be exhaustive).
   * @param string[] $unexpected_params
   *   A list of parameter names that are not expected in the request.
   * @param array $actual_params
   *   The actual Solr request parameters to check.
   */
  protected function assertSolrRequestMatches(
    string $expected_q,
    bool $q_exact,
    array $expected_params,
    array $unexpected_params,
    array $actual_params
  ): void {
    $actual_q = $actual_params['q'];
    if ($q_exact) {
      $this->assertEquals($expected_q, $actual_q);
    }
    else {
      $this->assertStringContainsString($expected_q, $actual_q);
      $this->assertNotEquals($expected_q, $actual_q);
    }
    $this->assertEquals($expected_params, array_intersect_key($actual_params, $expected_params));
    foreach ($unexpected_params as $param) {
      $this->assertArrayNotHasKey($param, $actual_params);
    }
  }

  /**
   * Asserts that exactly the given search results appear on the current page.
   *
   * @param int[]|null $results
   *   The results to expect in the Solr response, as their index in
   *   $this->entities. Or NULL to expect all items.
   */
  protected function assertCurrentPageContainsSearchResults(?array $results = NULL): void {
    $assert = $this->assertSession();
    $results = array_flip($results ?? array_keys($this->entities));
    foreach (array_intersect_key($this->entities, $results) as $result) {
      $assert->pageTextContains("default | {$result->label()}");
    }
    foreach (array_diff_key($this->entities, $results) as $result) {
      $assert->pageTextNotContains("default | {$result->label()}");
    }
  }

  /**
   * Retrieves the text contents of a descendant of the given element.
   *
   * @param \Behat\Mink\Element\NodeElement $element
   *   The element.
   * @param string $css_selector
   *   The CSS selector defining the descendant to look for.
   *
   * @return string|null
   *   The text contents of the descendant, or NULL if it couldn't be found.
   */
  protected function getElementText(NodeElement $element, string $css_selector): ?string {
    $element = $element->find('css', $css_selector);
    return $element ? $element->getText() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function clickLink($label, $index = 0): void {
    parent::clickLink($label, $index);

    $this->logPageChange("Clicked link with label \"$label\" (#$index)");
  }

  /**
   * Saves the current page contents to the debug HTML output, if enabled.
   *
   * Used since the base class unfortunately does not always re-save the HTML
   * when the page changes.
   *
   * @param string $label
   *   (optional) An explanation of the user action to print at the top of the
   *   saved HTML output.
   */
  protected function logPageChange(string $label = ''): void {
    // @see \Drupal\Tests\UiHelperTrait::click()
    $should_log = $this->htmlOutputEnabled
      && (!method_exists($this, 'isTestUsingGuzzleClient') || !$this->isTestUsingGuzzleClient())
      && !($this->getSession()->getDriver() instanceof GoutteDriver);
    if ($should_log) {
      $html_output = '';
      if ($label !== '') {
        $html_output .= htmlspecialchars($label) . '<hr />';
      }
      $current_url = htmlspecialchars($this->getSession()->getCurrentUrl());
      $html_output .= "Ending URL: $current_url";
      $html_output .= "<hr />{$this->getSession()->getPage()->getContent()}";
      $html_output .= $this->getHtmlOutputHeaders();
      $this->htmlOutput($html_output);
    }
  }

}
