<?php

declare(strict_types=1);

namespace Drupal\searchstax\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\search_api\Display\DisplayPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for changing the module's settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The plugin manager search api display.
   *
   * @var \Drupal\search_api\Display\DisplayPluginManagerInterface
   */
  protected DisplayPluginManagerInterface $displayPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $form = parent::create($container);

    $form->setEntityTypeManager($container->get('entity_type.manager'));
    $form->setDisplayPluginManager($container->get('plugin.manager.search_api.display'));
    $form->setModuleHandler($container->get('module_handler'));
    $form->setLoggerFactory($container->get('logger.factory'));

    return $form;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager ?? \Drupal::service('entity_type.manager');
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The new entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): SettingsForm {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Retrieves the plugin manager search api display.
   *
   * @return \Drupal\search_api\Display\DisplayPluginManagerInterface
   *   The plugin manager search api display.
   */
  public function getDisplayPluginManager(): DisplayPluginManagerInterface {
    return $this->displayPluginManager ?? \Drupal::service('plugin.manager.search_api.display');
  }

  /**
   * Sets the plugin manager search api display.
   *
   * @param \Drupal\search_api\Display\DisplayPluginManagerInterface $display_plugin_manager
   *   The new plugin manager search api display.
   *
   * @return $this
   */
  public function setDisplayPluginManager(DisplayPluginManagerInterface $display_plugin_manager): self {
    $this->displayPluginManager = $display_plugin_manager;
    return $this;
  }

  /**
   * Retrieves the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  public function getModuleHandler(): ModuleHandlerInterface {
    return $this->moduleHandler ?? \Drupal::service('module_handler');
  }

  /**
   * Sets the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The new module handler.
   *
   * @return $this
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler): self {
    $this->moduleHandler = $module_handler;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'searchstax_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'searchstax.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('searchstax.settings');

    $form['js_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Javascript library version'),
      '#description' => $this->t('The version of the SearchStax Studio tracking library to use for your site. You should usually prefer newer versions, but temporarily using an old version might be necessary in case you have custom additions to the tracking code on your site.'),
      '#options' => [
        '2' => '2',
        '3' => '3',
      ],
      '#default_value' => $config->get('js_version'),
    ];

    $form['analytics_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Analytics URL'),
      '#description' => $this->t("The SearchStudio analytics URL associated with this website. You can find this in your App's settings, under \"Analytics API\". Only available for Javascript library version 3 or higher."),
      '#default_value' => $config->get('analytics_url'),
      '#states' => [
        'visible' => [
          ':input[name="js_version"]' => [
            'value' => '3',
          ],
        ],
      ],
    ];

    $form['analytics_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global analytics key'),
      '#description' => $this->t('The SearchStudio analytics key associated with this website. You can override this on a per-search basis below, if you have more than one search app configured for this site.'),
      '#default_value' => $config->get('analytics_key'),
    ];

    $form['search_specific_analytics_keys'] = [
      '#type' => 'details',
      '#title' => $this->t('Search-specific analytics keys'),
      '#description' => $this->t('If you have multiple search apps configured in SearchStax for this website, please enter the analytics key to use for each individual search below. Leave a field blank to use the global key set above for that search – or disable tracking for a search, in case you have left the global key empty, too.'),
      '#tree' => TRUE,
    ];
    $displays = $this->getDisplayPluginManager()->getInstances();
    foreach ($displays as $id => $display) {
      $form['search_specific_analytics_keys'][$id] = [
        '#type' => 'textfield',
        '#title' => $display->label(),
        '#default_value' => $config->get("search_specific_analytics_keys.$id"),
      ];
    }

    try {
      $roles = $this->getEntityTypeManager()->getStorage('user_role')
        ->loadMultiple();
      $role_options = [];
      foreach ($roles as $id => $role) {
        $role_options[$id] = $role->label();
      }
      $form['untracked_roles'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Do not track these roles'),
        '#description' => $this->t('Select roles for which you want to disable tracking of search behavior.'),
        '#options' => $role_options,
        '#default_value' => $config->get('untracked_roles') ?: [],
      ];
    }
    catch (\Exception $e) {
      // @todo Remove once we depend on Drupal 10.1+.
      if (method_exists(Error::class, 'logException')) {
        Error::logException($this->logger('searchstax'), $e);
      }
      else {
        /* @noinspection PhpUndefinedFunctionInspection */
        watchdog_exception('searchstax', $e);
      }
    }

    if (\Drupal::moduleHandler()->moduleExists('search_api_autocomplete')) {
      $form['autosuggest_core'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Auto-suggest core'),
        '#description' => $this->t('The “core” part of your SearchStudio auto-suggest URL. This is the second-to-last part of the URL, between “solr” and “emsuggest”: [DOMAIN]/solr/[CORE]/emsuggest. (Only needed if you want to use auto-suggest.)'),
        '#default_value' => $config->get('autosuggest_core'),
      ];
    }
    // @todo Check whether /emselect is available before allowing to enable?
    $form['searches_via_searchstudio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Re-route searches through SearchStudio'),
      '#description' => $this->t('Re-route all Solr searches through the SearchStudio search handler, to immediately apply adjustments you make in SearchStudio.'),
      '#default_value' => (bool) $config->get('searches_via_searchstudio'),
    ];
    $form['configure_via_searchstudio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Configure searches via SearchStudio'),
      '#description' => $this->t('If this option is enabled, search settings configured in Drupal will be ignored and the settings configured in SearchStudio will take effect. You can configure which settings exactly will be affected. Fulltext keys, filters and paging parameters are always passed to the Solr server and are never affected by this setting.'),
      '#default_value' => (bool) $config->get('configure_via_searchstudio'),
      '#states' => [
        'visible' => [
          ':input[name="searches_via_searchstudio"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['discard_parameters'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Which Drupal search settings should be ignored?'),
      '#description' => $this->t('The selected Drupal settings will be ignored in the search request sent to SearchStax. Instead, the settings made within SearchStudio will take effect. (Otherwise, the Drupal settings will override the ones in SearchStudio.) This has no effect when there are no Drupal settings for the specific component – for instance, when spellchecking has not been enabled.'),
      '#options' => [
        'keys' => $this->t('Parse mode and searched fields'),
        'highlight' => $this->t('Highlighting settings'),
        'spellcheck' => $this->t('Spellcheck settings'),
        'sort' => $this->t('Sorts'),
        'facets' => $this->t('Facets'),
      ],
      'facets' => [
        '#description' => $this->t('This will in most cases not work correctly with the Facets module, so custom code handling the returned facets will be needed.'),
      ],
      '#default_value' => $config->get('discard_parameters'),
      '#states' => [
        'visible' => [
          ':input[name="searches_via_searchstudio"]' => ['checked' => TRUE],
          ':input[name="configure_via_searchstudio"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    // @todo Replace strpos() with str_contains() once we depend on PHP 8+.
    /* @noinspection PhpStrFunctionsInspection */
    if (!empty($form['autosuggest_core'])
        && strpos($form_state->getValue('autosuggest_core'), '/') !== FALSE) {
      $vars = ['@name' => $form['autosuggest_core']['#title']];
      $message = $this->t('Please enter @name without any slashes (/).', $vars);
      $form_state->setError($form['autosuggest_core'], $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $untracked_roles = $form_state->getValue('untracked_roles');
    $untracked_roles = array_keys(array_filter($untracked_roles));
    $discard_parameters = array_keys(array_filter($form_state->getValue('discard_parameters')));
    $this->config('searchstax.settings')
      ->set('js_version', $form_state->getValue('js_version'))
      ->set('analytics_url', $form_state->getValue('analytics_url'))
      ->set('analytics_key', $form_state->getValue('analytics_key'))
      ->set('search_specific_analytics_keys', array_filter($form_state->getValue('search_specific_analytics_keys') ?: []))
      ->set('untracked_roles', $untracked_roles)
      ->set('autosuggest_core', $form_state->getValue('autosuggest_core'))
      ->set('searches_via_searchstudio', $form_state->getValue('searches_via_searchstudio'))
      ->set('configure_via_searchstudio', $form_state->getValue('configure_via_searchstudio'))
      ->set('discard_parameters', $discard_parameters)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
