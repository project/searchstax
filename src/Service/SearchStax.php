<?php

declare(strict_types=1);

namespace Drupal\searchstax\Service;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\search_api\ParseMode\ParseModePluginManager;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSet;
use Drupal\search_api\SearchApiException;
use Solarium\Core\Query\QueryInterface as SolariumQueryInterface;
use Solarium\QueryType\Select\Query\Query as SolariumSelectQuery;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides utility methods for the SearchStax module.
 */
class SearchStax implements SearchStaxServiceInterface {

  // @todo Use constructor property promotion once we depend on PHP 8+.

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The current user account.
   */
  protected AccountInterface $currentUser;

  /**
   * The language manager.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * The parse mode plugin manager.
   */
  protected ParseModePluginManager $parseModePluginManager;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\search_api\ParseMode\ParseModePluginManager $parse_mode_plugin_manager
   *   The parse mode plugin manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    LanguageManagerInterface $language_manager,
    RequestStack $request_stack,
    ParseModePluginManager $parse_mode_plugin_manager
  ) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
    $this->parseModePluginManager = $parse_mode_plugin_manager;
  }

  /**
   * Retrieves the module's configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The module's configuration.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->configFactory->get('searchstax.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function isTrackingDisabled(?RefinableCacheableDependencyInterface $cache_metadata = NULL): bool {
    $config = $this->getConfig();
    if ($cache_metadata) {
      $cache_metadata->addCacheableDependency($config);
    }
    $roles = $config->get('untracked_roles');
    if (!$roles) {
      return FALSE;
    }
    if ($cache_metadata) {
      $cache_metadata->addCacheContexts(['user.roles']);
    }
    return (bool) array_intersect($this->currentUser->getRoles(), $roles);
  }

  /**
   * {@inheritdoc}
   */
  public function addTracking(ResultSet $result_set, array &$build = [], ?string $keys = NULL): bool {
    $cache = CacheableMetadata::createFromRenderArray($build);

    // Double-check that tracking isn't disabled for the current user.
    if ($this->isTrackingDisabled($cache)) {
      $cache->applyTo($build);
      return FALSE;
    }

    // Extract keys if not passed already.
    $query = $result_set->getQuery();
    $keys = $keys ?? $this->getQueryKeys($query);
    if ($keys === NULL) {
      $cache->applyTo($build);
      return FALSE;
    }

    // Get the analytics key. Return early if it is not set.
    $config = $this->getConfig();
    $cache->addCacheableDependency($config);
    $config_key = 'search_specific_analytics_keys.' . $query->getSearchId();
    $analytics_key = $config->get($config_key) ?: $config->get('analytics_key');
    if (!$analytics_key) {
      $cache->applyTo($build);
      return FALSE;
    }

    $offset = $query->getOption('offset', 0);
    $results = array_values($result_set->getResultItems());
    $urls = [];
    foreach ($results as $i => $item) {
      try {
        $url = $item->getDatasource()->getItemUrl($item->getOriginalObject());
      }
      catch (SearchApiException $ignored) {
        continue;
      }
      if (!$url) {
        continue;
      }
      $url = $url->toString(TRUE);
      $cache->addCacheableDependency($url);
      $urls[] = [
        'url' => $url->getGeneratedUrl(),
        'position' => $offset + $i + 1,
      ];
    }

    $page_no = 1;
    $limit = $query->getOption('limit');
    if ($limit) {
      $page_no += floor($offset / $limit);
    }
    $search_info = [
      'query' => $keys,
      'shownHits' => count($result_set->getResultItems()),
      'totalHits' => $result_set->getResultCount(),
      'pageNo' => $page_no,
      'tracked' => FALSE,
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
    ];
    // For Solr searches, we also support the "latency" key as the query time
    // is already returned by Solr.
    $solr_response = $result_set->getExtraData('search_api_solr_response');
    $latency = $solr_response['responseHeader']['QTime'] ?? NULL;
    if ($latency !== NULL) {
      $search_info['latency'] = $latency;
    }

    foreach (array_values($result_set->getResultItems()) as $i => $item) {
      $item_info = [
        'cDocId' => $item->getId(),
        'position' => $offset + $i + 1,
      ];
      try {
        $object = $item->getOriginalObject(FALSE);
        if ($object) {
          $item_info['cDocTitle'] = $item->getDatasource()
            ->getItemLabel($object);
          $cacheable_object = $object;
          if (!($cacheable_object instanceof CacheableDependencyInterface)) {
            $cacheable_object = $object->getValue();
          }
          $cache->addCacheableDependency($cacheable_object);
        }
      }
      catch (SearchApiException $ignored) {
        // The item label is not that important.
      }
      $search_info['impressions'][] = $item_info;
    }

    $tracking_info = [
      'key' => $analytics_key,
    ];

    // Add the session ID, if there is a session yet. Otherwise, we don't want
    // to create one, as that would probably end up in the static page cache,
    // which would mean different anonymous visitors would all get the same
    // session ID assigned for that page (but still a new one for each
    // different page). Instead, if no session key is passed via the settings,
    // we manually create a random new one in Javascript.
    // @todo This is not persistent for anonymous users. Is this what we want?
    if ($this->requestStack->getCurrentRequest()->hasPreviousSession()) {
      $tracking_info['session'] = $this->requestStack->getCurrentRequest()->getSession()->getId();
      $cache->addCacheContexts(['session']);
    }

    // For non-anonymous users, also include the user ID.
    $uid = $this->currentUser->id();
    $cache->addCacheContexts(['user']);
    if ($uid) {
      $tracking_info['user'] = $uid;
    }

    $build['#attached']['library'][] = 'searchstax/searchstax.tracking';
    $settings = [
      'analytics_url' => rtrim($config->get('analytics_url'), '/'),
      'js_version' => $config->get('js_version'),
      'tracking_base_data' => $tracking_info,
      'searches' => [
        $query->getSearchId() => $search_info,
      ],
    ];
    if ($urls) {
      $settings['results_urls'][$query->getSearchId()] = $urls;
    }
    $build['#attached']['drupalSettings']['searchstax'] = $settings;
    $cache->applyTo($build);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSolrQuery(SolariumQueryInterface $solarium_query, QueryInterface $query): void {
    try {
      // If this is not a select query, don't alter it.
      if (($solarium_query->getHandler() ?: 'select') !== 'select'
        || !($solarium_query instanceof SolariumSelectQuery)) {
        return;
      }

      // Include the response header so we can use it to obtain the latency.
      $solarium_query->setOmitHeader(FALSE);

      // Check whether this is even a SearchStax server, and not a regular Solr
      // server.
      $solr_config = $query->getIndex()->getServerInstance()->getBackendConfig();
      if (!$this->isSearchstaxSolr($solr_config)) {
        return;
      }

      // Check whether user enabled re-routing searches to SearchStudio.
      $config = $this->getConfig();
      $enabled = $config->get('searches_via_searchstudio');
      if (!$enabled) {
        return;
      }

      $solarium_query->setHandler('emselect');

      // Check whether we should also remove most search params set by Drupal
      // and simplify search keys so SearchStudio settings will take effect.
      if (!$config->get('configure_via_searchstudio')) {
        // Set the "defType=lucene" query parameter (unless the "defType"
        // parameter is already set).
        if (empty($solarium_query->getParams()['defType'])) {
          $solarium_query->addParam('defType', 'lucene');
        }
        return;
      }
      $discard_parameters = (array) $config->get('discard_parameters');

      if (in_array('keys', $discard_parameters)) {
        // Make sure search keywords are passed as-is, not re-written.
        /** @var \Drupal\search_api\ParseMode\ParseModeInterface $direct */
        $direct = $this->parseModePluginManager->createInstance('direct');
        $query->setParseMode($direct);
        // For some reason, "direct" parse mode is ignored unless
        // defType=edismax is explicitly set.
        $solarium_query->addParam('defType', 'edismax');

        // However, as we don't want query fields ("qf" parameter) passed, we
        // don't actually want the edismax component on the Solarium query.
        // (This way, the Solr backend plugin will also remove the "defType"
        // parameter we set above for us.)
        $solarium_query->removeComponent(SolariumSelectQuery::COMPONENT_EDISMAX);
      }
      elseif (empty($solarium_query->getParams()['defType'])) {
        $solarium_query->addParam('defType', 'lucene');
      }

      // Also remove other feature components, sorts and facets, as configured.
      if (in_array('facets', $discard_parameters)) {
        $solarium_query->removeComponent(SolariumSelectQuery::COMPONENT_FACETSET);
      }
      if (in_array('highlight', $discard_parameters)) {
        $solarium_query->removeComponent(SolariumSelectQuery::COMPONENT_HIGHLIGHTING);
      }
      if (in_array('sort', $discard_parameters)) {
        $solarium_query->clearSorts();
      }
      if (in_array('spellcheck', $discard_parameters)) {
        $solarium_query->removeComponent(SolariumSelectQuery::COMPONENT_SPELLCHECK);
      }
    }
    catch (SearchApiException | PluginException $ignored) {
      // Very unlikely, but just ignore if it happens – will cause other errors
      // later in the page request.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isSearchstaxSolr(array $config): bool {
    $host = $config['connector_config']['host'] ?? '';
    $host_parts = explode('.', $host);
    $n = count($host_parts);
    // If host is "localhost" or similar,
    // $host_parts just contains a single item.
    if ($n < 2) {
      return FALSE;
    }
    $host_suffix = $host_parts[$n - 2] . '.' . $host_parts[$n - 1];
    return in_array($host_suffix, ['searchstax.com', 'searchstax.co']);
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryKeys(QueryInterface $query): ?string {
    if (!$query->getDisplayPlugin()) {
      return NULL;
    }
    $keys = $query->getOriginalKeys();
    if (is_array($keys)) {
      $keys = $this->stringifyComplexKeys($keys);
    }
    return $keys;
  }

  /**
   * Extracts search keywords from an array.
   *
   * @param array $keys
   *   The keywords, in the array format described in
   *   \Drupal\search_api\ParseMode\ParseModeInterface::parseInput().
   *
   * @return string|null
   *   A string representing the keywords. Or NULL if there were no keywords.
   */
  protected function stringifyComplexKeys(array $keys): ?string {
    $extracted = [];
    foreach ($keys as $i => $key) {
      if (!Element::child($i)) {
        continue;
      }
      if (is_string($key) && $key !== '') {
        $extracted[] = $key;
      }
      elseif (is_array($key)) {
        $key = $this->stringifyComplexKeys($key);
        if (!$key) {
          continue;
        }
        if (substr($key, 0, 2) !== '-(') {
          $key = "($key)";
        }
        $extracted[] = $key;
      }
    }

    if (!$extracted) {
      return NULL;
    }

    $glue = ($keys['#conjunction'] ?? 'AND') == 'AND' ? ' ' : ' OR ';
    $extracted_keys = implode($glue, $extracted);
    if (!empty($keys['#negation'])) {
      $extracted_keys = "-($extracted_keys)";
    }
    return $extracted_keys;
  }

}
