<?php

declare(strict_types=1);

namespace Drupal\searchstax\Service;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSet;
use Solarium\Core\Query\QueryInterface as SolariumQueryInterface;

/**
 * Provides an interface for the SearchStax utility service.
 */
interface SearchStaxServiceInterface {

  /**
   * Determines whether tracking is disabled for the current user.
   *
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $cache_metadata
   *   (optional) A cache metadata object to record any cacheable dependencies
   *   of this check.
   *
   * @return bool
   *   TRUE if tracking is disabled for the current user role, FALSE otherwise.
   */
  public function isTrackingDisabled(?RefinableCacheableDependencyInterface $cache_metadata = NULL): bool;

  /**
   * Adds search results URL information to a result set.
   *
   * @param \Drupal\search_api\Query\ResultSet $result_set
   *   The result set to process.
   * @param array $build
   *   (optional) If specified, a build array to which to add cache metadata.
   * @param string|null $keys
   *   (optional) The search keys for which to track. Will be extracted from the
   *   query if not passed.
   *
   * @return bool
   *   TRUE if tracking was added to the build array, FALSE otherwise.
   */
  public function addTracking(ResultSet $result_set, array &$build = [], ?string $keys = NULL): bool;

  /**
   * Alters a Search API Solr query.
   *
   * If enabled and appropriate, re-routes all search requests through
   * SearchStudio's /emselect handler.
   *
   * Events were only introduced in the 4.2.x release cycle of the Search API
   * Solr module, so depending on the module version this will be either called
   * by our alter hook implementation or by our event subscriber.
   *
   * @param \Solarium\Core\Query\QueryInterface $solarium_query
   *   The Solr query to alter.
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The underlying Search API query.
   *
   * @see \Drupal\searchstax\EventSubscriber\SearchSubscriber::preQueryAlter()
   * @see searchstax_search_api_solr_query_alter()
   */
  public function alterSolrQuery(SolariumQueryInterface $solarium_query, QueryInterface $query): void;

  /**
   * Determines whether the Solr server uses a SearchStax server.
   *
   * @param array $config
   *   The Solr server's configuration array.
   *
   * @return bool
   *   TRUE if the configuration points to a SearchStax Solr server, FALSE
   *   otherwise.
   */
  public function isSearchstaxSolr(array $config): bool;

  /**
   * Retrieves the fulltext keywords of the given query.
   *
   * Will return NULL for queries that shouldn't be tracked. (Queries without an
   * attached search display shouldn't be tracked.)
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The search query.
   *
   * @return string|null
   *   The fulltext keywords (converted to a string, if necessary), if there are
   *   any and the query should be tracked. NULL otherwise.
   */
  public function getQueryKeys(QueryInterface $query): ?string;

}
