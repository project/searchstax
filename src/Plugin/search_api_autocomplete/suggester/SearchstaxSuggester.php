<?php

declare(strict_types=1);

namespace Drupal\searchstax\Plugin\search_api_autocomplete\suggester;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_autocomplete\SearchApiAutocompleteException;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\search_api_autocomplete\Suggester\SuggesterPluginBase;
use Drupal\search_api_autocomplete\Suggestion\Suggestion;
use Drupal\search_api_autocomplete\Suggestion\SuggestionFactory;
use Drupal\search_api_solr\SolrBackendInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides autocomplete suggestions using the Searchstax "/emsuggest" endpoint.
 *
 * @SearchApiAutocompleteSuggester(
 *   id = "searchstax",
 *   label = @Translation("SearchStax"),
 *   description = @Translation("Uses the SearchStudio ""/emsuggest"" endpoint to create suggestions."),
 * )
 */
class SearchstaxSuggester extends SuggesterPluginBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logging channel to use.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function supportsSearch(SearchInterface $search): bool {
    if (!\Drupal::config('searchstax.settings')->get('autosuggest_core')) {
      return FALSE;
    }
    try {
      $backend = static::getSolrBackend($search->getIndex());
      if (!$backend) {
        return FALSE;
      }
      return \Drupal::getContainer()->get('searchstax.utility')
        ->isSearchstaxSolr($backend->getConfiguration());
    }
    // @todo Remove variable once we depend on PHP 8+.
    /* @noinspection PhpUnusedLocalVariableInspection */
    catch (SearchApiException | SearchApiAutocompleteException $ignored) {
      // Ignore, just return FALSE.
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $plugin->setConfigFactory($container->get('config.factory'));
    $plugin->setLogger($container->get('logger.factory')->get('searchstax'));

    return $plugin;
  }

  /**
   * Retrieves the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   */
  public function getConfigFactory(): ConfigFactoryInterface {
    return $this->configFactory ?? \Drupal::service('config.factory');
  }

  /**
   * Sets the config factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The new config factory.
   *
   * @return $this
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory): self {
    $this->configFactory = $config_factory;
    return $this;
  }

  /**
   * Retrieves the logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger(): LoggerInterface {
    return $this->logger ?? \Drupal::logger('searchstax');
  }

  /**
   * Sets the logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The new logger.
   *
   * @return $this
   */
  public function setLogger(LoggerInterface $logger): self {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, $incomplete_key, $user_input): array {
    $suggestions = [];
    try {
      $backend = static::getSolrBackend($query->getIndex());
      if (!$backend) {
        return $suggestions;
      }
      $connector = $backend->getSolrConnector();
      $suggester_query = $connector->getSuggesterQuery();
      $suggester_query->setHandler('emsuggest');
      $suggester_query->addParam('q', $user_input);
      $endpoint = clone $connector->getEndpoint();
      $core = $this->getConfigFactory()->get('searchstax.settings')
        ->get('autosuggest_core');
      if ($core) {
        $endpoint->setCore($core);
      }
      $response = $connector->execute($suggester_query, $endpoint);
      $data = $response->getData();
      // Actual data is nested three levels down (suggest.suggester1[user_input]
      // – but no point in hard-coding that).
      while (count($data) === 1) {
        $tmp = reset($data);
        if (!is_array($tmp)) {
          break;
        }
        $data = $tmp;
      }

      $suggestion_factory = new SuggestionFactory($user_input);
      foreach ($data['suggestions'] ?? [] as $suggestion) {
        if (!empty($suggestion['term'])) {
          $term = strip_tags($suggestion['term']);
          if (preg_match('#(.*)<b>(.*)</b>(.*)#', $suggestion['term'], $m)) {
            $suggestions[] = (new Suggestion($term))
              ->setSuggestionPrefix($m[1])
              ->setUserInput($m[2])
              ->setSuggestionSuffix($m[3]);
          }
          else {
            $suggestions[] = $suggestion_factory->createFromSuggestedKeys($term);
          }
        }
      }
    }
    catch (SearchApiException $e) {
      // @todo Remove once we depend on Drupal 10.1+.
      if (method_exists(Error::class, 'logException')) {
        Error::logException($this->getLogger(), $e, '%type while fetching autocomplete suggestions: @message in %function (line %line of %file).');
      }
      else {
        /* @noinspection PhpUndefinedFunctionInspection */
        watchdog_exception('searchstax', $e, '%type while fetching autocomplete suggestions: @message in %function (line %line of %file).');
      }
    }
    return $suggestions;
  }

  /**
   * Retrieves the Solr backend plugin associated with the given index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index.
   *
   * @return \Drupal\search_api_solr\SolrBackendInterface|null
   *   The backend plugin of the Solr server attached to the index. Or NULL if
   *   the index isn't attached to a Solr server.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred while retrieving the server backend plugin.
   */
  protected static function getSolrBackend(IndexInterface $index): ?SolrBackendInterface {
    if (!$index->hasValidServer()) {
      return NULL;
    }
    $server = $index->getServerInstance();
    $backend = $server->getBackend();
    if ($backend instanceof SolrBackendInterface) {
      return $backend;
    }
    return NULL;
  }

}
